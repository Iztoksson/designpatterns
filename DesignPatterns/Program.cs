﻿namespace DesignPatterns
{
    using System;
    using System.Threading.Tasks;

    public static class Program
    {
        public static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("## Common design patterns resource. ##");
                Console.WriteLine("Input number for test:");
                Console.WriteLine("[1] ObserverWithEvents");
                Console.WriteLine("[2] ObserverWithSubscription");
                Console.WriteLine("[3] Singleton Thread Safe.");
                Console.WriteLine("[4] Builder (Fluent).");
                Console.WriteLine("[X] Exit.");

                string inputCommand = Console.ReadLine().ToUpper();

                if (inputCommand == "X") return;
                else if (inputCommand == "1") Behavioral.TestObserverWithEvents.DoTest();
                else if (inputCommand == "2") Behavioral.TestObserverWithSubscription.DoTest();
                else if (inputCommand == "3") Creational.TestSingleton.DoTest();
                else if (inputCommand == "4") Creational.TestBuilderFluent.DoTest();
                else Console.WriteLine("Unknown command.");

                Console.WriteLine();
            }
        }
    }
}