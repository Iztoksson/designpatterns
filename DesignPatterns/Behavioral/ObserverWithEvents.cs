﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral
{
    public static class TestObserverWithEvents
    {
        public static void DoTest()
        {
            var observable = new Observable();
            var observerWithEvents = new ObserverWithEvents();

            observable.NotifyAll += observerWithEvents.HandleNotification;

            observable.DoWork();
        }
    }

    public class ObserverWithEvents
    {
        public void HandleNotification(object sender, EventArgs e)
        {
            Console.WriteLine($"{sender} notified me that he did some work.");
        }
    }

    public class Observable
    {
        public event EventHandler NotifyAll;

        public void DoWork()
        {
            Console.WriteLine("Doing some work and will notify all my observers after I'm done.");
            // Make a safe call by using ?, if there are no subscribers then NotifyAll is null!
            NotifyAll.Invoke(this, EventArgs.Empty);
        }
    }
}