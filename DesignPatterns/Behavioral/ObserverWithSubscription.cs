﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Behavioral
{
    public static class TestObserverWithSubscription
    {
        public static void DoTest()
        {
            var tweetFeed = new TweetFeed();
            tweetFeed.SubscribeObserver(new SmsObserver());
            tweetFeed.SubscribeObserver(new EmailObserver());

            var snapchatFeed = new SnapchatFeed();
            snapchatFeed.SubscribeObserver(new EmailObserver());

            tweetFeed.TweetSomething("I'm tweeting this message, dispatch it wherever you like.");
            snapchatFeed.SnapSomething("I took a derpy photo, here it is.");
        }
    }

    internal abstract class Feed
    {
        private List<IObserver> _observers = new List<IObserver>();

        public void SubscribeObserver(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void UnsubscribeObserver(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void NotifyAll(string message)
        {
            foreach (IObserver observer in _observers)
            {
                observer.Update(message);
            }
        }
    }

    internal class TweetFeed : Feed
    {
        internal void TweetSomething(string message)
        {
            base.NotifyAll(message);
        }
    }

    internal class SnapchatFeed : Feed
    {
        internal void SnapSomething(string message)
        {
            base.NotifyAll(message);
        }
    }

    internal interface IObserver
    {
        void Update(string message);
    }

    internal class SmsObserver : IObserver
    {
        public void Update(string message)
        {
            Console.WriteLine($"{this} got the message which reads: '{message}'");
        }
    }

    internal class EmailObserver : IObserver
    {
        public void Update(string message)
        {
            Console.WriteLine($"{this} got the message which reads: '{message}'");
        }
    }
}