﻿namespace DesignPatterns.Creational
{
    using System;
    using System.Collections.Generic;

    public static class TestBuilderFluent
    {
        public static void DoTest()
        {
            var builder = new Builder();
            var director = new Director(builder);

            var product = director.MakeProductForEveryone();
            product.ListSpecifications();

            product = director.MakeProductForSomeoneSpecial();
            product.ListSpecifications();
        }
    }

    public class Director
    {
        private readonly IBuilder _builder;

        public Director(IBuilder builder)
        {
            this._builder = builder;
        }

        public Product MakeProductForEveryone()
        {
            return _builder
                .AddPartOne()
                .AddPartTwo()
                .AddPartThree()
                .Build();
        }

        public Product MakeProductForSomeoneSpecial()
        {
            return _builder
                .AddPartOne()
                .AddPartTwo()
                .AddPartThree()
                .AddBonus()
                .Build();
        }
    }

    public interface IBuilder
    {
        Builder AddPartOne();

        Builder AddPartTwo();

        Builder AddPartThree();

        Builder AddBonus();

        void Reset();

        Product Build();
    }

    public class Builder : IBuilder
    {
        private Product _product = new Product();

        public Builder AddBonus()
        {
            _product.AddSpecification("Contains Bonus for that one special person!");
            return this;
        }

        public Builder AddPartOne()
        {
            _product.AddSpecification("Contains part one.");
            return this;
        }

        public Builder AddPartThree()
        {
            _product.AddSpecification("Contains part three.");
            return this;
        }

        public Builder AddPartTwo()
        {
            _product.AddSpecification("Contains part two.");
            return this;
        }

        public void Reset()
        {
            this._product = new Product();
        }

        public Product Build()
        {
            Product result = _product;

            this.Reset();

            return result;
        }
    }

    public class Product
    {
        private List<string> specifications = new List<string>();

        public void AddSpecification(string specification)
        {
            specifications.Add(specification);
        }

        public void ListSpecifications()
        {
            foreach (string specification in specifications)
            {
                Console.WriteLine($"Specification: {specification}");
            }
            Console.WriteLine();
        }
    }
}