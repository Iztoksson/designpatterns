﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational
{
    public static class TestSingleton
    {
        public static void DoTest()
        {
            var s1 = Singleton.Instance;
            var s2 = Singleton.Instance;

            if (s1 == s2)
            {
                Console.WriteLine($"{nameof(s1)} and {nameof(s2)} are the same instance.");
            }

            s1.TestProperty = "Setting property for first instance.";
            s2.TestProperty = "Setting property for second instance.";

            Console.WriteLine($"Value for first instance: \"{s1.TestProperty}\" and Value for second instance: \"{s2.TestProperty}\".");
        }
    }

    public class Singleton
    {
        private static Singleton instance = null;
        private static readonly object padlock = new object();

        private Singleton()
        {
        }

        public static Singleton Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Singleton();
                    }
                    return instance;
                }
            }
        }

        public string TestProperty { get; set; }
    }
}