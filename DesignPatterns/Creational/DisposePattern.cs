﻿namespace DesignPatterns.Creational
{
    public class DisposePattern : System.IDisposable
    {
        private System.Data.IDbConnection _connection; // Example resource!
        private System.Data.IDbTransaction _transaction; // Example resource!

        private bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // Dispose unmanaged and any other resources here.

                    // Example resources!
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~DisposePattern()
        {
            Dispose(false);
        }
    }
}